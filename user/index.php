<?php
session_start();
include '../admin/conn-db.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SMP MUHAMMADIYAH 9 SIDAYU</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <style>
        button, .button{
            background: #F3F3F3;
            border: 1px solid #DCDCDC;
            border-radius: 2px;
            color: #444444;
            cursor: pointer;
            display: inline-block;
            font: 700 11px Tahoma, Arial, sans-serif;
            margin-right: 10px;
            padding: 7px 12px 7px 12px;
            position: relative;
            text-decoration: none;
            text-shadow: 0px 1px 0px #FFFFFF;
        }
        button:hover, .button:hover{
            border-bottom-color: #999999;
            border-left-color: #999999;
            border-right-color: #999999;
            border-top-color: #999999;
            color: #333333;
            text-decoration: none;
        }
    </style>
    <?php
        if($_GET['simpan_artikel']){
            $update = mysql_query("update artikel set judul = '".$_POST['judul']."', isi = '".$_POST['isi']."' where id = '".$_GET['simpan_artikel']."' ");
            if ($update) {
		echo "<script>
                        alert('Data berhasil diubah');
                        window.location='index.php';
                     </script>";
            }else {
               echo "<script>
                        alert('Data gagal diubah');
                        window.location='index.php';
                     </script>";
            }
        }
        function artikel($id, $tipe){
            $sql          = "select * from artikel where id = '".$id."'";
            $query        = mysql_query($sql);
            $data_artikel = mysql_fetch_array($query);
            if($tipe == 'judul'){
                echo $data_artikel['judul'];
            }else if($tipe == 'isi'){
                echo $data_artikel['isi'];
            }
        }
        if($_GET['portofolio'] == 'simpan'){
            
	$allowed = array('jpg','jpeg','png','gif','JPG');
	$extension = pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION);

            if(!in_array(strtolower($extension), $allowed)){
                    echo "<script>
                            alert('Data gagal diunggah, ekstensi tidak sesuai.');
                            window.location='index.php';
                         </script>";
            }else{
                $dirfile = 'images/portfolio/thumb/'.$_FILES['foto']['name'];
                if(move_uploaded_file($_FILES['foto']['tmp_name'], $dirfile)){

                    $q = mysql_query("insert into portofolio values(null, '".$_POST['kategori']."', '".$dirfile."', '".$_POST['ket']."')");
                    if ($q) {
                        echo "<script>
                                alert('Data berhasil disimpan');
                                window.location='index.php';
                             </script>";
                    }else {
                       echo "<script>
                                alert('Data gagal disimpan');
                                window.location='index.php';
                             </script>";
                    }
                }
            }
        }else if($_GET['portofolio'] == 'hapus'){
            $sql = "select foto from portofolio where id = '".$_GET['id']."' ";
            $query = mysql_query($sql);
            $array = mysql_fetch_array($query);
            
            unlink($array['foto']);
            
            $delete = mysql_query("delete from portofolio where id = '".$_GET['id']."' ");
            if ($delete) {
                echo "<script>
                      alert('Data berhasil dihapus');
                      window.location='index.php';
                      </script>";
            }else {
                echo "<script>
                       alert('Data gagal dihapus');
                       window.location='index.php';
                      </script>";
            }
        }
    ?>
</head><!--/head-->

<body data-spy="scroll" data-target="#navbar" data-offset="0">
    <header id="header" role="banner">
        <div class="container">
            <div id="navbar" class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../index.php?proses=login"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#main-slider"><i class="icon-home"></i></a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#contact">Raport Online</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header><!--/#header-->

    <section id="main-slider" class="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <div class="container">
                    <div class="carousel-content">
                        <h1>SMP Muhammadiyah 9 Sidayu</h1>
                        <p class="lead">Xeon is the best free onepage responsive theme available arround the globe Download it right now for free</p>
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item">
                <div class="container">
                    <div class="carousel-content">
                        <h1>ShapeBootstrap.net</h1>
                        <p class="lead">Download free but 100% premium quaility twitter Bootstrap based WordPress and HTML themes <br>from shapebootstrap.net</p>
                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.carousel-inner-->
        <a class="prev" href="#main-slider" data-slide="prev"><i class="icon-angle-left"></i></a>
        <a class="next" href="#main-slider" data-slide="next"><i class="icon-angle-right"></i></a>
    </section><!--/#main-slider-->

    <section id="services">
        <div class="container">
            <div class="box first">
                <div class="row">
                        <?php
                            if($_GET['artikel']){
                                $sql          = "select * from artikel where id = '".$_GET['artikel']."'";
                                $query        = mysql_query($sql);
                                $data_artikel = mysql_fetch_array($query);
                                $judul   = $data_artikel['judul'];
                                $isi     = $data_artikel['isi'];
                                
                            ?>
                            <form action="index.php?simpan_artikel=<?=$_GET['artikel']?>" method="post">
                                <table style="margin: 15px">
                                    <tr>
                                        <td colspan="3" style="text-align: center; font-size: 36px; font-weight: bold">Ubah Data Artikel</td>
                                    </tr>
                                    <tr>
                                        <td>Judul Artikel</td>
                                        <td>:</td>
                                        <td><input type="text" name="judul" value="<?=$judul?>" style="padding-left: 5px"/></td>
                                    </tr>
                                    <tr>
                                        <td>Isi Artikel</td>
                                        <td>:</td>
                                        <td><textarea name="isi" style="padding-left: 5px; width: 380px; height: 140px;"><?=$isi?></textarea></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><button type="submit" name="proses">Ubah Data</button><a href="index.php"><button type="button" name="proses">Batal</button></a></td>
                                    </tr>
                                </table>
                            </form>
                            <?php
                                }else{
                            ?>
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                                <?php
                                     if($_SESSION['s_level'] == 'Admin'){
                                ?>
                                <a href="index.php?artikel=1" class="btn btn-primary btn-lg">Ubah Konten</a>
                                <?php
                                    }else{
                                ?>
                                <i class="icon-apple icon-md icon-color1"></i>
                                <?php
                                    }
                                ?>
                                <h4><?=artikel(1, 'judul')?></h4>
                                <p><?=artikel(1, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a href="index.php?artikel=2" class="btn btn-primary btn-lg">Ubah Konten</a>
                            <?php
                                }else{
                            ?>
                            <i class="icon-android icon-md icon-color2"></i>
                            <?php
                                }
                            ?>
                                <h4><?=artikel(2, 'judul')?></h4>
                                <p><?=artikel(2, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a href="index.php?artikel=3" class="btn btn-primary btn-lg">Ubah Konten</a>
                            <?php
                                }else{
                            ?>
                            <i class="icon-windows icon-md icon-color3"></i>
                            <?php
                                }
                            ?>
                                <h4><?=artikel(3, 'judul')?></h4>
                                <p><?=artikel(3, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a href="index.php?artikel=4" class="btn btn-primary btn-lg">Ubah Konten</a>
                            <?php
                                }else{
                            ?>
                            <i class="icon-html5 icon-md icon-color4"></i>
                            <?php
                                }
                            ?>
                                <h4><?=artikel(4, 'judul')?></h4>
                                <p><?=artikel(4, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a href="index.php?artikel=5" class="btn btn-primary btn-lg">Ubah Konten</a>
                            <?php
                                }else{
                            ?>
                            <i class="icon-css3 icon-md icon-color5"></i>
                            <?php
                                }
                            ?>
                                <h4><?=artikel(5, 'judul')?></h4>
                                <p><?=artikel(5, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                    <div class="col-md-4 col-sm-6">
                        <div class="center">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a href="index.php?artikel=6" class="btn btn-primary btn-lg">Ubah Konten</a>
                            <?php
                                }else{
                            ?>
                            <i class="icon-thumbs-up icon-md icon-color6"></i>
                            <?php
                                }
                            ?>
                                <h4><?=artikel(6, 'judul')?></h4>
                                <p><?=artikel(6, 'isi')?></p>
                        </div>
                    </div><!--/.col-md-4-->
                            <?php
                                }
                            ?>
                </div><!--/.row-->
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#services-->

    <section id="portfolio">
        <div class="container">
            <div class="box">
                <?php
                    if($_GET['portofolio'] == 'input'){
                ?>
                <form action="index.php?portofolio=simpan" method="post" enctype="multipart/form-data">
                <table id="luar">
                <tr>
                <td colspan="3" style="text-align: center; font-size: 36px; font-weight: bold">UPLOAD FOTO</td>
                </tr>
                <tr>
                <td>Kategory</td>
                <td>:</td>
                <td><select name="kategori">
                        <option value="ekstra">Extra Kulikuler</option>
                        <option value="kegiatan">Kegiatan</option>
                        <option value="sekolah">Sekolah</option>
                    </select>
                </td>
                </tr>
                <tr>
                <td>Pilih file</td>
                <td>:</td>
                <td><input type="file" name="foto"></td>
                </tr>
                <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td><input type="text" name="ket" size="30"></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" name="proses">Unggah</button><a href="index.php"><button type="button" name="proses">Batal</button></a></td>
                </tr>
                </table>
                </form>
                <?php
                    }else{
                    ?>
                <div class="center gap">
                    <h2>Portfolio</h2>
                    <p class="lead">Dibawah ini adalah Foto kegiatan, Ektra Kulikuler dll yang ada di SMP Muhammadiyah 9 Sidayu.</p>
                </div><!--/.center-->
                <ul class="portfolio-filter">
                    <li><a class="btn btn-primary active" href="#" data-filter="*">All</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".ekstra">Extra Kulikuler</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".kegiatan">Kegiatan</a></li>
                    <li><a class="btn btn-primary" href="#" data-filter=".sekolah">Sekolah</a></li>
                </ul><!--/#portfolio-filter-->
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                            <a class="btn btn-primary btn-lg" href="index.php?portofolio=input">Tambah Foto</a>
                            <?php
                                }
                                ?>
                <ul class="portfolio-items col-4">
                    <?php
                        $sql = "select * from portofolio";
                        $query = mysql_query($sql);
                    while($q = mysql_fetch_array($query)){    
                    ?>
                    <li class="portfolio-item <?=$q['kategori']?>">
                        <div class="item-inner">
                            <div class="portfolio-image">
                                <img src="<?=$q['foto']?>" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-danger" title="Lorem ipsum dolor sit amet" href="<?=$q['foto']?>"><i class="icon-eye-open"></i></a>             
                                </div>
                            </div>
                            <h5><?=$q['ket']?> <a href="index.php?portofolio=hapus&id=<?=$q['id']?>">
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                            ?>
                                    <button type="button" name="hapus">Hapus</button></a></h5>
                            <?php
                                }
                                ?>
                        </div>
                    </li><!--/.portfolio-item-->
                    <?php
                    }
                    ?>
                </ul>
                <?php
                    }
                    ?>
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#portfolio-->

    <section id="contact">
        <div class="container">
            <div class="box">
                <div class="center">
                    <h2>Raport Online</h2>
                    <p class="lead">Silahkan Login Untuk Melihat Raport Online.</p>
                    <table style="padding: 5px; margin: 15px; height: 100px">
                        <tr>
                            <td hight="35px">User ID</td>
                            <td hight="35px">:</td>
                            <td hight="35px"><input type="text" name="userid"></td>
                        </tr>
                        <tr>
                            <td hight="35px">Password</td>
                            <td hight="35px">:</td>
                            <td hight="35px"><input type="password" name="passwd"></td>
                        </tr>
                        <tr>
                            <td hight="35px" colspan="3"><button type="submit" name="proses">Login</button></td>
                        </tr>
                    </table>
                </div>
                <div class="gap"></div>
                
            </div><!--/.box-->
        </div><!--/.container-->
    </section><!--/#about-us-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
                </div>
                <div class="col-sm-6">
                    <img class="pull-right" src="images/shapebootstrap.png" alt="ShapeBootstrap" title="ShapeBootstrap">
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>