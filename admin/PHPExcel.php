<?php
 
/** Error reporting */
error_reporting(E_ALL);
session_start();
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
 
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
 
date_default_timezone_set('Asia/Jakarta');
 
/** Include PHPExcel */
require_once 'Classes/PHPExcel.php';
 
// Membuat script koneksi
require_once 'conn-db.php';
 
// Membuat documen excel baru
$objPHPExcel = new PHPExcel();
 
// Set Properti Documen excel yang akan dibuat
$objPHPExcel->getProperties()->setCreator("...")
                             ->setLastModifiedBy("...")
                             ->setTitle("Report Data Pegawai Office 2007 XLSX")
                             ->setSubject("Data Pegawai Office 2007 XLSX")
                             ->setDescription("Detail Data Kepegawaian")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Data Nilai");

$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'000000')
);
$style_border = array(
    'borders' => array(
        'allborders' => 
				array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
					)
    )
);

$styleArray = array( 
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
             	'vertical'   => PHPExcel_Style_Alignment::VERTICAL_TOP,
             	'rotation'   => 0,
			),
			'borders' => 
			array( 'allborders' => 
				array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
					), 
				), 
			);
 
		// melakukan pengaturan pada header kolom
		$fontHeader = array( 
			'font' => array(
				'bold' => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
             	'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
             	'rotation'   => 0,
			),
			'fill' => array(
            	'type' => PHPExcel_Style_Fill::FILL_SOLID,
            	'color' => array('rgb' => 'cccccc')
        	),
			'borders' => 
			array( 'allborders' => 
				array( 'style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('argb' => '00000000'), 
					), 
				), 
		);

		$objWorksheet = $objPHPExcel->getActiveSheet();
                
$array = mysql_fetch_array(mysql_query("SELECT * FROM daftar_nilai a, ulangan_harian b, tugas c, ujian d, remidi e, murid f, kelas g, mapel h, standar_nilai i
    WHERE a.`id_UHarian`= b.`id_UHarian` 
    AND a.`id_tugas`=c.`id_tugas` 
    AND a.`id_ujian`=d.`id_ujian` 
    AND a.`id_remidi`=e.`id_remidi` 
    AND a.`id_siswa`=f.`id_siswa` 
    AND f.`id_kelas`=g.`id_kelas` 
    AND a.`id_mapel`=h.`id_mapel` 
    AND h.`id_mapel`=i.`id_mapel`
    AND f.id_kelas ='".$_POST['kelas']."'
    AND a.id_mapel ='".$_POST['id_mapel']."'
    AND a.tapel = '".$_POST['tapel']."'
    AND a.semester = '".$_POST['semester']."'"));

$objPHPExcel->getActiveSheet()->mergeCells('A1:X1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
$objPHPExcel->getActiveSheet()->mergeCells('A3:C3');
$objPHPExcel->getActiveSheet()->mergeCells('A4:C4');
$objPHPExcel->getActiveSheet()->mergeCells('R2:T2');
$objPHPExcel->getActiveSheet()->mergeCells('R3:T3');
$objPHPExcel->getActiveSheet()->mergeCells('W2:X2');
$objPHPExcel->getActiveSheet()->mergeCells('W3:X3');


//========== Data Pribadi
$objPHPExcel->getActiveSheet()->getStyle('A1:X1')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,));
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DAFTAR KUMPULAN NILAI SISWA');
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Mata Pelajaran');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Kelas');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'KKM');
$objPHPExcel->getActiveSheet()->setCellValue('D2', ': '.$array['mapel']);
$objPHPExcel->getActiveSheet()->setCellValue('D3', ': '.$array['kelas']);
$objPHPExcel->getActiveSheet()->setCellValue('D4', ': '.$array['standar']);
$objPHPExcel->getActiveSheet()->setCellValue('R2', 'Semester');
$objPHPExcel->getActiveSheet()->setCellValue('R3', 'Tahun Pelajaran');
$objPHPExcel->getActiveSheet()->setCellValue('W2', ': '.$array['semester']);
$objPHPExcel->getActiveSheet()->setCellValue('W3', ': '.$array['tapel']);


//set table header
//Tabel akan kita mulai dari Kolom B10 dan seterusnya
$objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Nomor');
$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Urt');
$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Induk');
$objPHPExcel->getActiveSheet()->mergeCells('C5:D6');
$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama Siswa');
$objPHPExcel->getActiveSheet()->mergeCells('E5:J5');
$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Ulangan Harian');
$objPHPExcel->getActiveSheet()->setCellValue('E6', '1');
$objPHPExcel->getActiveSheet()->setCellValue('F6', '2');
$objPHPExcel->getActiveSheet()->setCellValue('G6', '3');
$objPHPExcel->getActiveSheet()->setCellValue('H6', '4');
$objPHPExcel->getActiveSheet()->setCellValue('I6', '5');
$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Rt2');
$objPHPExcel->getActiveSheet()->mergeCells('K5:P5');
$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Tugas-Tugas');
$objPHPExcel->getActiveSheet()->setCellValue('K6', '1');
$objPHPExcel->getActiveSheet()->setCellValue('L6', '2');
$objPHPExcel->getActiveSheet()->setCellValue('M6', '3');
$objPHPExcel->getActiveSheet()->setCellValue('N6', '4');
$objPHPExcel->getActiveSheet()->setCellValue('O6', '5');
$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Rt2');
$objPHPExcel->getActiveSheet()->mergeCells('Q5:Q6');
$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'RTH');
$objPHPExcel->getActiveSheet()->mergeCells('R5:T5');
$objPHPExcel->getActiveSheet()->setCellValue('R5', 'NB');
$objPHPExcel->getActiveSheet()->setCellValue('R6', 'UTS');
$objPHPExcel->getActiveSheet()->setCellValue('S6', 'SMT');
$objPHPExcel->getActiveSheet()->setCellValue('T6', 'Rt2');
$objPHPExcel->getActiveSheet()->mergeCells('U5:U6');
$objPHPExcel->getActiveSheet()->setCellValue('U5', 'NA');
$objPHPExcel->getActiveSheet()->mergeCells('V5:W5');
$objPHPExcel->getActiveSheet()->setCellValue('V5', 'Remidi');
$objPHPExcel->getActiveSheet()->setCellValue('V6', 'R1');
$objPHPExcel->getActiveSheet()->setCellValue('W6', 'R2');
$objPHPExcel->getActiveSheet()->mergeCells('X5:X6');
$objPHPExcel->getActiveSheet()->setCellValue('X5', 'NR');
$objWorksheet->getStyle('A5:X6')->applyFromArray($fontHeader);


// Add some data
    $array_Ulangan = mysql_fetch_array(mysql_query("SELECT ROUND(max(uh_1),0) as max_1, ROUND(max(uh_2),0) as max_2, ROUND(max(uh_3),0) as max_3, ROUND(max(uh_4),0) as max_4, ROUND(max(uh_5),0) as max_5, ROUND(min(uh_1),0) as min_1, ROUND(MIN(uh_2),0) AS min_2, ROUND(MIN(uh_3),0) AS min_3, ROUND(MIN(uh_4),0) AS min_4, ROUND(MIN(uh_5),0) AS min_5, ROUND(avg(uh_1),0) as avg_1, ROUND(AVG(uh_2),0) AS avg_2, ROUND(AVG(uh_3),0) AS avg_3, ROUND(AVG(uh_4),0) AS avg_4, round(AVG(uh_5),0) AS avg_5 from daftar_nilai a, ulangan_harian b, murid f, kelas g, mapel h 
WHERE a.`id_UHarian`= b.`id_UHarian` 
AND a.`id_siswa`=f.`id_siswa` 
  AND f.`id_kelas`=g.`id_kelas` 
    AND a.`id_mapel`=h.`id_mapel`AND f.id_kelas ='".$_POST['kelas']."'
    AND a.id_mapel ='".$_POST['id_mapel']."'
    AND a.tapel = '".$_POST['tapel']."'
    AND a.semester = '".$_POST['semester']."'"));
    
$query = mysql_query("SELECT * FROM daftar_nilai a, ulangan_harian b, tugas c, ujian d, remidi e, murid f, kelas g, mapel h, standar_nilai i
    WHERE a.`id_UHarian`= b.`id_UHarian` 
    AND a.`id_tugas`=c.`id_tugas` 
    AND a.`id_ujian`=d.`id_ujian` 
    AND a.`id_remidi`=e.`id_remidi` 
    AND a.`id_siswa`=f.`id_siswa` 
    AND f.`id_kelas`=g.`id_kelas` 
    AND a.`id_mapel`=h.`id_mapel` 
    AND h.`id_mapel`=i.`id_mapel`
    AND f.id_kelas ='".$_POST['kelas']."'
    AND a.id_mapel ='".$_POST['id_mapel']."'
    AND a.tapel = '".$_POST['tapel']."'
    AND a.semester = '".$_POST['semester']."'");
//start data from row 11
$i = 7;
$no= 1;
while($data=mysql_fetch_array($query)){
    
    $rt_uh = ($data['uh_1']+$data['uh_2']+$data['uh_3']+$data['uh_4']+$data['uh_5'])/5;
    $rt_tgs = ($data['tgs_1']+$data['tgs_2']+$data['tgs_3']+$data['tgs_4']+$data['tgs_5'])/5;
    $rth = 0;
    $rt_ujian = $data['uts']+$data['smt']/2; 
    $na = 0;
    $nr = 0;
    
    
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no.'.');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $data['no_induk']);
    $objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':D'.$i);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $data['nm_siswa']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $data['uh_1']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $data['uh_2']);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $data['uh_3']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $data['uh_4']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $data['uh_5']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $rt_uh);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $data['tgs_1']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $data['tgs_2']);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $data['tgs_3']);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $data['tgs_4']);
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $data['tgs_5']);
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $rt_tgs);
    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $rth);
    $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $data['uts']);
    $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $data['smt']);
    $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $rt_ujian);
    $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $na);
    $objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $data['r1']);
    $objPHPExcel->getActiveSheet()->setCellValue('W'.$i, $data['r2']);
    $objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $nr);
    $i++;
    $no++;
}
$column = 'X'.($i-1);


$objPHPExcel->getActiveSheet()->mergeCells('A'.($i).':D'.($i));
$objPHPExcel->getActiveSheet()->setCellValue('A'.($i), 'Nilai Tertinggi');
$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+1).':D'.($i+1));
$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+1), 'Nilai Terendah');
$objPHPExcel->getActiveSheet()->mergeCells('A'.($i+2).':D'.($i+2));
$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+2), 'Nilai Rata-Rata');

$objWorksheet->getStyle('A7:'.$column)->applyFromArray($style_border);
$objPHPExcel->getActiveSheet()->getStyle('E7:'.$column)->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
$objPHPExcel->getActiveSheet()->getStyle('A7:A'.($i-1))->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,));
$objPHPExcel->getActiveSheet()->getStyle('B7:B'.($i-1))->getAlignment()->applyFromArray(
    array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));

		
		
//Mengatur lebar cell pada documen excel
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(8);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Data Pegawai');

// Set sheet yang aktif pada documen excel
$objPHPExcel->setActiveSheetIndex(0);
 
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Report Data Pegawai.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');