<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
	<title>Penilaian Siswa</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/navi.css" media="screen" />
	<link href="img/ico.png" rel="shortcut icon" />
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

	<link rel="stylesheet" type="text/css" href="css/easyui.css">
	<link rel="stylesheet" type="text/css" href="css/icon.css">
	 <script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.easyui.min.js"></script>

	<script type="text/javascript">
		$(function(){
			$(".box .h_title").not(this).next("ul").hide("normal");
			$(".box .h_title").not(this).next("#home").show("normal");
			$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
		});			
		function getkey(e){
			if (window.event)
			   return window.event.keyCode;
			else if (e)
			   return e.which;
			else
			   return null;
		}
		
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return (d<10?('0'+d):d)+'/'+(m<10?('0'+m):m)+'/'+y;
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('/'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
	</script>
        <script language="javascript">
            function getkey(e){
                if (window.event)
                   return window.event.keyCode;
                else if (e)
                   return e.which;
                else
                   return null;
            }
            function goodchars(e, goods, field){
                var key, keychar;
                key = getkey(e);
                if (key == null) return true;

                keychar = String.fromCharCode(key);
                keychar = keychar.toLowerCase();
                goods = goods.toLowerCase();

                // check goodkeys
                if (goods.indexOf(keychar) != -1)
                    return true;
                // control keys
                if ( key==null || key==0 || key==8 || key==9 || key==27 )
                   return true;

                if (key == 13) {
                    var i;
                    for (i = 0; i < field.form.elements.length; i++)
                        if (field == field.form.elements[i])
                            break;
                    i = (i + 1) % field.form.elements.length;
                    field.form.elements[i].focus();
                    return false;
                    };
                // else return false
                return false;
            }
        </script>
	<!-- DATE PICKER -->
	<script type="text/javascript">
		$(function() {
		$( "#input" ).datepicker({
			dateFormat : "dd/mm/yy",
			changeMonth: true,
			changeYear: true
		});
		});
	</script>
</head>

<body>
	<?php
	$menu='halaman_utama';
        if (isset($_GET['menu'])) {
		$menu=$_GET['menu'];
	}
	?>
<div class="wrap">
	<div id="header">
		<div id="top">
                    <div class="left">				
                        <p><a href="index.php" id="logo"><img src="img/welcome.png" style="z-index:-1;"  width="100%"></a></p>
			</div>			
		</div>
            <div id="nav">
			<div class='right'>
                            <b>
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                                ?>
			<ul style="margin-top: -30px">
                            <?php
                                }else{
                                ?>
			<ul style="margin-top:130px">
                            <?php
                                }
                                ?>
                            <li class="upp"><a href="index.php?menu=halaman_utama">Home</a></li>
                            <li class="upp"><a href="../user/index.php">Website</a></li>
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                                ?>
                            <li class="upp"><a>Master Data</a>
                                <ul>
                                    <li>&#8250; <a href="index.php?menu=data_siswa">Data Siswa</a></li>
                                    <li>&#8250; <a href="index.php?menu=siswa_keluar">Data Siswa Keluar</a></li>
                                    <li>&#8250; <a href="index.php?menu=data_orangtua">Data Orang Tua Siswa</a></li>
                                    <li>&#8250; <a href="index.php?menu=perkembangan_siswa">Data Perkembangan Siswa</a></li>
                                    <li>&#8250; <a href="index.php?menu=nilai">Nilai</a></li>
                                    <li>&#8250; <a href="index.php?menu=kelas">Kelas</a></li>
                                    <li>&#8250; <a href="index.php?menu=mapel">Mata Pelajaran</a></li>
                                    <li>&#8250; <a href="index.php?menu=standar">KKM</a></li>
                                    <li>&#8250; <a href="index.php?menu=slide_show">Slide Image</a></li>
                                </ul>
                            </li>
                            <?php
                                }
                                ?>
                            <?php
                                if($_SESSION['s_level'] == 'Admin'){
                                ?>
                            <li class="upp"><a>Laporan</a>
                                <ul>
                                    <li>&#8250; <a href="index.php?menu=slide_show">Foto</a></li>
                                </ul>
                            </li>
                            <?php
                                }
                                ?>
                <?php
                if($_SESSION['s_level'] != ''){
                ?>
                <li class="upp"><a href="logout.php">Logout</a></li>
                <?php
                }else{
                ?>
                            <li class="upp"><a href="index.php?menu=halaman_utama&login=true">Login</a></li>
                <?php
                }
                ?>
			</ul>
                            </b>
			</div>
            
		</div>
        
	</div>
	<div id="content">				
		<div id="main">						
			<div class="clear"></div>								
				<!-- CALL FILE PAGE -->
<!--<iframe frameborder="0"  src="<?$menu.".php"?>" style="width:1335px; height:620px;"></iframe>-->
				<?php				
				if ($menu == 'halaman_utama' | $menu=='data_siswa' | $menu=='kelas' | $menu=='mapel' | $menu=='slide_show' | $menu=='standar' | $menu=='nilai' | $menu=='siswa_keluar' | $menu=='data_orangtua' | $menu=='perkembangan_siswa') {
					include $menu.".php";					
				}else{
					echo "DORR...";
				}			
				?>							
		</div>  <!-- END DIV ID MAIN -->		
		<div class="clear"></div>
	</div> <!-- END DIV ID CONTENT -->

	<div id="footer">
		<div class="left">			
			Copyright © 2015. Universitas Muhammadiyah Gresik
		</div>		
	</div>
</div>

</body>
</html>
