<?php
session_start();
function time_elapsed_string($ptime)
{
    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}
?>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
<script>
	function makemein() {
		var user = document.getElementById('userid').value;
		var pass = document.getElementById('passwd').value;
		if(user!='' && pass!='') {
		     return true;
		}
		else {
		     alert('Isi User Name dan Password terlebih dahulu');
		     return false;
		}
	}

window.setTimeout("chat()",1500); 
function chat() { 
setTimeout("chat()",1500); 
$('#notification').load("notification.php");
} 
</script>
<style>
    #frame-login{
        background: #ffffff;
	border: 1px solid #CCC;
	color: #848484;
	margin: 0 0 20px 0;
        height: 380px;
	width: 800px;
	float: right;
	border-radius:5px 5px 5px 5px;
	-moz-border-radius:5px 5px 5px 5px;
	-webkit-border-radius:5px 5px 5px 5px;
    }
    .lengkunglogin
{
   height:14px;
   background-color:#DBDBDB;
   color:#000;
   text-shadow:#000;
border-radius:5px 5px 0px 0px;
-moz-border-radius:5px 5px 0px 0px;
-webkit-border-radius:5px 5px 0px 0px;
border:1px solid #25814F;
padding:10px;
}
.bawahlogin
{
   height:200px;
   background-color:#FFF;
   color:#000;
border-radius:0px 0px 5px 5px;
-moz-border-radius:0px 0px 5px 5px;
-webkit-border-radius:0px 0px 5px 5px;
border:1px solid #25814F;
padding:90px 0px 0px 260px;
}
.login
{
   background-color:#FFF;
   color:#000;
   font-family:"Arial", Gadget, sans-serif;
   font-size:18px;
   height:40px;
border-radius:0px 0px 0px 0px;
-moz-border-radius:0px 0px 0px 0px;
-webkit-border-radius:0px 0px 0px 0px;
border-bottom:1px solid #25814F;
padding-top:15px;
}
.kotak_login
{
   font-family:"Arial", Gadget, sans-serif;
   font-size:18px;
border-radius:3px 3px 3px 3px;
-moz-border-radius:3px 3px 3px 3px;
-webkit-border-radius:3px 3px 3px 3px;
border:0px solid #25814F;
padding:25px 25px 10px 25px;
background-color:#CCC;
}
.text_login
{
   display:block;
   left:10px;
   font-family:"Arial", Gadget, sans-serif;
   font-size:14px;
border-radius:3px 3px 3px 3px;
-moz-border-radius:3px 3px 3px 3px;
-webkit-border-radius:3px 3px 3px 3px;
border:0px solid #25814F;
background-color:#FFF;
}
.logingambar
{
   display:block;
   content:' ';
   top:10px;
   left:20px;
   right:100px;
}
.logintext
{
   display:block;
   content:' ';
   top:10px;
   left:50px;
   right:100px;
   color:#000;
   font-family:Arial, Helvetica, sans-serif;
}
</style>
<!--		<div class="lengkungnotif" >
	        <div class="logingambar" ><img src="images/notif.png" width="20" height="20"/></div>
            <div class="logintext" >Notification</div>
        </div>
        <div class="bawahnotif" >
        	<div id="notification"></div>
        </div>-->
        <?php
//		if($_SESSION['s_user']==''){
		?>
<div id="frame-login">
        <div class="lengkunglogin" >
            <div class="logingambar" ><img src="images/login.png" width="20" height="20"/></div>
            <div class="logintext" >Form Login</div>
        </div>
        <div class="bawahlogin" >
            <div class="login" >
            <marquee width='640' scrolldelay='20' scrollamount='1' truespeed='100' onMouseOver='this.stop()' onMouseOut='this.start()'>Silahkan Login</marquee>
            </div>
            <div class="kotak_login">
            <form name="flogin" id="flogin" method="post" onSubmit="return makemein()" action="actlogin.php">
            <table width="300">
            	<tr height="35">
                	<td>User ID</td>
                    <td>:</td>
                    <td><input type="text" name="userid" /></td>
                </tr>
                <tr height="35">
                	<td>Password</td>
                    <td>:</td>
                    <td><input type="password" name="passwd" /></td>
                </tr>
                <tr height="35">
                	<td></td>
                    <td></td>
                    <td><input type="submit" name="submit" value="Login" /></td>
                </tr>
            </table>
            </form>
            </div>
            <div class="text_login">
            <table width="250">
            	<tr>
                	<td colspan="2">Keterangan :</td>
                </tr>
                <tr>
                	<td valign="top">-</td>
                	<td>Untuk menggunakan menu anda harus login terlebih dahulu.</td>
                </tr>
                <tr>
                	<td valign="top">-</td>
                	<td>Jika anda belum terdaftar sebagai user anda harus mendaftar terlebih dahulu dengan menghubungi admin.</td>
                </tr>
            </table>
            </div>
        </div>
    </div>
        <?php
//		}else{
		?>
<!--		<div class="bawahbasic" >
        Selamat Datang Di System Evaluasi
        </div>-->
        <?php
//		}
		?>