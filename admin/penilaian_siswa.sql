/*
SQLyog Ultimate v9.02 
MySQL - 5.0.51a : Database - penilaian_siswa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`penilaian_siswa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `penilaian_siswa`;

/*Table structure for table `daftar_nilai` */

DROP TABLE IF EXISTS `daftar_nilai`;

CREATE TABLE `daftar_nilai` (
  `id_nilai` int(11) default NULL,
  `tapel` int(11) default NULL,
  `semester_i` int(11) default NULL,
  `semester_ii` int(11) default NULL,
  `agama_islam` varchar(50) default NULL,
  `hadits` varchar(50) default NULL,
  `akidah_akhlaq` varchar(50) default NULL,
  `fikih` varchar(50) default NULL,
  `ski` varchar(50) default NULL,
  `pkn` varchar(50) default NULL,
  `b_indonesia` varchar(50) default NULL,
  `b_arab` varchar(50) default NULL,
  `mtk` varchar(50) default NULL,
  `ipa` varchar(50) default NULL,
  `ips` varchar(50) default NULL,
  `sbk` varchar(50) default NULL,
  `penjas` varchar(50) default NULL,
  `muatan_lokal` varchar(50) default NULL,
  `b_inggris` varchar(50) default NULL,
  `b_daerah` varchar(50) default NULL,
  `plh` varchar(50) default NULL,
  `mapel_baru` varchar(50) default NULL,
  `jml` varchar(50) default NULL,
  `rata_rata` varchar(50) default NULL,
  `peringkat` varchar(50) default NULL,
  `naik_tidak` varchar(50) default NULL,
  `no_induk` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `daftar_nilai` */

/*Table structure for table `keterangan_ortu` */

DROP TABLE IF EXISTS `keterangan_ortu`;

CREATE TABLE `keterangan_ortu` (
  `id_ortu` int(11) default NULL,
  `nm_ayah` varchar(100) default NULL,
  `pendidikan_ayah` varchar(50) default NULL,
  `pekerjaan_ayah` varchar(50) default NULL,
  `alamat_ayah` varchar(50) default NULL,
  `nm_ibu` varchar(100) default NULL,
  `pendidikan_ibu` varchar(50) default NULL,
  `pekerjaan_ibu` varchar(50) default NULL,
  `alamat_ibu` varchar(50) default NULL,
  `nm_wali` varchar(100) default NULL,
  `pendidikan_wali` varchar(50) default NULL,
  `pekerjaan_wali` varchar(50) default NULL,
  `alamat_wali` varchar(50) default NULL,
  `hubungan_keluarga` varchar(50) default NULL,
  `no_induk` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `keterangan_ortu` */

/*Table structure for table `meninggalkan_madrasah` */

DROP TABLE IF EXISTS `meninggalkan_madrasah`;

CREATE TABLE `meninggalkan_madrasah` (
  `id_madrasah` int(11) default NULL,
  `thn_tamat` varchar(50) default NULL,
  `no_sttb` varchar(50) default NULL,
  `lanjutan_madrasah` varchar(50) default NULL,
  `dari_tingkat` varchar(50) default NULL,
  `ke_sekolah` varchar(50) default NULL,
  `tgl_pindah` varchar(50) default NULL,
  `tgl_keluar` varchar(50) default NULL,
  `alasan_keluar` varchar(50) default NULL,
  `no_induk` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `meninggalkan_madrasah` */

/*Table structure for table `murid` */

DROP TABLE IF EXISTS `murid`;

CREATE TABLE `murid` (
  `id_siswa` int(11) NOT NULL auto_increment,
  `no_induk` int(11) default NULL,
  `nisn` varchar(11) default NULL,
  `nm_siswa` varchar(100) default NULL,
  `jk` varchar(5) default NULL,
  `ttl` varchar(50) default NULL,
  `alamat` varchar(50) default NULL,
  `no_tlp` varchar(50) default NULL,
  `jarak` varchar(50) default NULL,
  `kewarganegaraan` varchar(50) default NULL,
  `kandung` varchar(50) default NULL,
  `tiri` varchar(50) default NULL,
  `angkat` varchar(50) default NULL,
  PRIMARY KEY  (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `murid` */

/*Table structure for table `perkembangan_murid` */

DROP TABLE IF EXISTS `perkembangan_murid`;

CREATE TABLE `perkembangan_murid` (
  `id_perkembangan` int(11) default NULL,
  `asal_murid` varchar(50) default NULL,
  `nm_tk` varchar(100) default NULL,
  `alamat` varchar(50) default NULL,
  `tanggal` varchar(50) default NULL,
  `no_sttb` varchar(50) default NULL,
  `nm_sekolah_asal` varchar(50) default NULL,
  `dari_tingkat` varchar(50) default NULL,
  `diterima_tgl` varchar(50) default NULL,
  `no_induk` varchar(50) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `perkembangan_murid` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
